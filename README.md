# Tees task.

[![Python Version](https://img.shields.io/badge/python-3.7-brightgreen.svg)](https://python.org)
[![Django Version](https://img.shields.io/badge/django-2.1-brightgreen.svg)](https://djangoproject.com)
[![Django Rest Framework Version](https://img.shields.io/badge/djangorestframework-3.11.0-brightgreen.svg)](https://www.django-rest-framework.org/)

## Running the Project Locally

First, clone the repository to your local machine:

```bash
git clone https://artuputra@bitbucket.org/selfrepository/tees.git
```

Install the requirements:

```bash
pip install -r requirements.txt
```

Finally, to run the development server, you need to move into tees_api folder:

```bash
cd tees_api
python manage.py runserver
```

And if to run simple recommendation system you shold enter recommendation system folder than type:
```bash
python rec_sys.py
```

The documentation of list API can be found on sample_test_api.json inside tees_api folder
The API endpoints will be available at **127.0.0.1:8000**.