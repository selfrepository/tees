import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle

product_descriptions = pd.read_csv('dataset/home-depot-product-search-relevance/product_descriptions.csv')

product_descriptions = product_descriptions.dropna()

vectorizer = TfidfVectorizer(stop_words='english')
X_vectorizer = vectorizer.fit_transform(product_descriptions["product_description"])

def print_cluster(i):
    print("Cluster %d:" % i),
    for ind in order_centroids[i, :10]:
        print(' %s' % terms[ind]),

true_k = 10

model = pickle.load(open("save.pkl", "rb"))
print("Top terms per cluster:")
order_centroids = model.cluster_centers_.argsort()[:, ::-1]
terms = vectorizer.get_feature_names()


def show_recommendations(product):
    print("Name of product input: ", product)
    Y = vectorizer.transform([product])
    prediction = model.predict(Y)
    print_cluster(prediction[0])

show_recommendations("tool")
