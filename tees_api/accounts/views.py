from django.contrib.auth import login
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.views import LoginView as KnoxLoginView
from rest_framework import generics, permissions, views
from knox.models import AuthToken
from .serializers import UserSerializer, RegisterSerializer
from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from django.contrib.auth.models import User
from .serializers import ChangePasswordSerializer
from rest_framework.permissions import IsAuthenticated
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
import pickle

# Register API
class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })

# Login API
class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginAPI, self).post(request, format=None)

# Get User API
class UserAPI(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user


# Change Password
class ChangePasswordView(generics.UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class cobaProcess():

    import os.path
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    # ratings_Beauty = os.path.join(BASE_DIR, "dataset/ratings_Beauty.csv")
    product_descriptions_csv = os.path.join(BASE_DIR, "dataset/home-depot-product-search-relevance/product_descriptions.csv")
    product_descriptions = pd.read_csv(product_descriptions_csv)

    product_descriptions = product_descriptions.dropna()

    vectorizer = TfidfVectorizer(stop_words='english')
    X_vectorizer = vectorizer.fit_transform(product_descriptions["product_description"])

    def print_cluster(self, i):
        print("Cluster %d:" % i),
        for ind in self.order_centroids[i, :10]:
            print(' %s' % self.terms[ind]),
        print

    true_k = 10

    model = pickle.load(open("save.pkl", "rb"))
    print("Top terms per cluster:")
    order_centroids = model.cluster_centers_.argsort()[:, ::-1]
    terms = vectorizer.get_feature_names()

    def show_recommendations(self, product):
        # print("Cluster ID:")
        Y = self.vectorizer.transform([product])
        prediction = self.model.predict(Y)
        # print(prediction)
        self.print_cluster(prediction[0])


    # def show_recommendations(self, product):
    #     # print("Cluster ID:")
    #     Y = self.vectorizer.transform([product])
    #     prediction = self.model.predict(Y)
    #     # print(prediction)
    #     self.print_cluster(prediction[0])

    # show_recommendations("cutting tool")
    # def process(self, x):
    #     # self.show_recommendations(x)
    #     self.show_recommendations("cutting tool")


class Rec_Sys(views.APIView):
    # permission_classes = [permissions.IsAuthenticated, ]
    def get(self, request):
        response = {
            'status': 'success',
        }
        return Response(response)


    def post(self, request, *args, **kwargs):
        cobaProcess()

        # import os.path
        # BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        # db_path = os.path.join(BASE_DIR, "save.pkl")
        # vectorizer = TfidfVectorizer(stop_words='english')
        # model = pickle.load(open(db_path, "rb"))
        # order_centroids = model.cluster_centers_.argsort()[:, ::-1]
        # terms = vectorizer.get_feature_names()
        # def print_cluster(i):
        #     print("Cluster %d:" % i),
        #     for ind in order_centroids[i, :10]:
        #         print(' %s' % terms[ind]),
        #
        # def show_recommendations(product):
        #     # print("Cluster ID:")
        #     Y = vectorizer.transform([product])
        #     prediction = model.predict(Y)
        #     # print(prediction)
        #     print_cluster(prediction[0])
        #
        # show_recommendations("tool")


        # print(request.data)
        # print((request.data['Authorization']))
        # print(args)
        # print(kwargs)
        usernames = 'oke'
        response = {
            'status': 'success',
            'data': [usernames]
        }
        return Response(response)




